import './App.css';
import ContactDetails from './components/ContactDetails/ContactDetails';
import Sidebar from './components/ContactsSidebar/ContactsSidebar';
import { ContactsProvider } from './ContactsContext'

function App() {
  return (
    <ContactsProvider >
      <main className="layout">
        <Sidebar />
        <ContactDetails />
      </main>
    </ContactsProvider>
  );
}

export default App;
