//const contactRequests = () => {
	export function getContacts() {
		return fetch('http://localhost:3004/contacts?deleted=false')
			.then(r => r.json())
			
	}

	export function createContact(contact) {
		return fetch('http://localhost:3004/contacts', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(contact)
		}).then(r => r.json())
	}

	export function deleteContact(id) {
		return fetch(`http://localhost:3004/contacts/${id}`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ deleted: true })
		}).then(r => r.json())
	}


