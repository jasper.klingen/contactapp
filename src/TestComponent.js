import React from 'react'
import { useContacts, useUpdateContacts } from './ContactsContext'

const TestComponent = () => {
    const contacts = useContacts()
    const addContact = useUpdateContacts()

    const elements = contacts.map(c => {
        return (
            <div key={c.id}>
                <p>{c.name}</p>
                <p>{c.id}</p>
            </div>
        )
    })

    return (
        <div>
            <h1>TestComponent</h1>
            <button onClick={() => addContact({
                id: 2,
                name: 'hallo 2',
                deleted: false
            })}>Add</button>
            {elements}
        </div>
    )
}

export default TestComponent