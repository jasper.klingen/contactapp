import React, { useContext, useEffect, useState } from 'react'
import {getContacts, /*createContact, deleteContact*/} from './contactRequests'


const ContactsContext = React.createContext()
const ContactsUpdateContext = React.createContext()

export const useContacts = () => {
    return useContext(ContactsContext)
}

export const useUpdateContacts = id => {
    return useContext(ContactsUpdateContext)
}

export const ContactsProvider = ({ children }) => {
    useEffect(() => {
        getContactsFromContext()
    }, []) 
    async function getContactsFromContext(){
        const DBcontacts = await getContacts()
            setContacts([...contacts,...DBcontacts])


    }
    const [contacts, setContacts] = useState([

    ])

    const addContact = contact => {
        console.log(contact);
        setContacts([...contacts, contact])
    }

    return (
        <ContactsContext.Provider value={contacts}>
            <ContactsUpdateContext.Provider value={addContact}>
                {children}
            </ContactsUpdateContext.Provider>
        </ContactsContext.Provider>
    )

}