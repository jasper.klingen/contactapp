import ContactListItem from './ContactListItem'

export function ContactList() {

    return (
        <ul>
            <ContactListItem />
        </ul>
    )
}

export default ContactList