import React from 'react'
import { useContacts, useUpdateContacts } from '../../ContactsContext'

const ContactListItem = () => {
    const contacts = useContacts()
    const addContact = useUpdateContacts()
    //console.log(contacts)
    let elements = []
    console.log(contacts)
    if (contacts) {
        elements = contacts.map(c => {

            return (
                <div key={c.id}>
                    <p>{c.name}</p>
                    <p>{c.id}</p>
                </div>
            )

        })

    }


    return (
        <div>
            <h1>Contactlist</h1>
            <button onClick={() => addContact({
                id: 2,
                name: 'hallo 2',
                deleted: false
            })}>Add</button>
            {elements}
        </div>
    )
}

export default ContactListItem