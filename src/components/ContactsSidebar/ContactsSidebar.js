// import ContactDetails from '../ContactDetails/ContactDetails'
import ContactList from '../ContactList/ContactList'

export function Sidebar() {

    return (
        <aside className="sidebar-container">
            <h3>Sidebar</h3>
            <ContactList />
        </aside>
    )
}

export default Sidebar